create table events(
   ID serial CONSTRAINT events_PK PRIMARY KEY,
   name varchar(255) NOT NULL,
   userID varchar(255) NOT NULL,
   source varchar(255) NOT NULL,
   status varchar(255) NULL
);

create table event_types(
   ID serial CONSTRAINT event_types_PK PRIMARY KEY,
   name varchar(255) NOT NULL,
   displayName varchar(255) NOT NULL
);

create table event_results(
   ID serial CONSTRAINT event_results_PK PRIMARY KEY,
   eventID INT NOT NULL REFERENCES events(ID),
   winner1Result int,
   winner2Result int,
   winner3Result int,
   looser1Result int,
   looser2Result int,
   looser3Result int
);

create table event_pools(
   ID serial CONSTRAINT event_pools_PK PRIMARY KEY,
   eventID INT NOT NULL REFERENCES events(ID),
   userID varchar(255) NOT NULL,
   source varchar(255) NOT NULL,
   name varchar(255) NOT NULL,
   eventTypeID INT NOT NULL REFERENCES event_types(ID),
   numberOfSets INT NOT NULL,
   numberOfSquarePerUser INT NOT NULL,
   status varchar(255) NOT NULL,
   uuid varchar(255) NOT NULL,
   chaturl varchar(512) NOT NULL
);

create table event_pool_square_claims(
   ID serial CONSTRAINT event_poolsquare_claims_PK PRIMARY KEY,
   eventPoolID INT NOT NULL REFERENCES event_pools(ID),
   rowNumber varchar(2) NOT NULL,
   columnNumber varchar(2) NOT NULL,
   memberName varchar(255) NOT NULL,
   memberID varchar(255) NOT NULL,
   source varchar(255) NOT NULL,
   constraint squares unique(eventPoolID, rowNumber, columnNumber)
);

create table event_pool_row_bets(
   ID serial CONSTRAINT event_pool_row_bets_PK PRIMARY KEY,
   eventPoolID INT NOT NULL REFERENCES event_pools(ID),
   rowNumber varchar(2) NOT NULL,
   grid1Value INT NULL,
   grid2Value INT NULL,
   grid3Value INT NULL,
   grid4Value INT NULL,
   grid5Value INT NULL,
   grid6Value INT NULL
);

create table event_pool_column_bets(
   ID serial CONSTRAINT event_pool_column_bets_PK PRIMARY KEY,
   eventPoolID INT NOT NULL REFERENCES event_pools(ID),
   columnNumber varchar(2) NOT NULL,
   grid1Value INT NULL,
   grid2Value INT NULL,
   grid3Value INT NULL,
   grid4Value INT NULL,
   grid5Value INT NULL,
   grid6Value INT NULL
);

create table number_of_sets(
   ID serial CONSTRAINT number_of_sets_PK PRIMARY KEY,
   name varchar(255) NOT NULL,
   value INT NOT NULL
);

create table pool_totals(
   ID serial CONSTRAINT event_pool_totals_PK PRIMARY KEY,
   eventPoolID INT NOT NULL REFERENCES event_pools(ID),
   memberName varchar(255) NOT NULL,
   memberID varchar(255) NOT NULL,
   source varchar(255) NOT NULL,
   total INT,
   round5Total INT,
   round6Total INT
);
