var db = require('./db.js');

module.exports = {

    generateMarchMadnessResults: function(result) {
        var id = result.rows[0]["id"];
        db.query('insert into event_results(eventid) values($1)', [id], function(result2) {
            for (var i = 1; i <= 3; i++) {
                var team1Result = db.madnessRandom(80, 120);
                var team2Result = db.madnessRandom(80, 120);
                console.log("Team1 Result - " + team1Result);
                console.log("Team2 Result - " + team2Result);
                if (team1Result == team2Result) {
                    team2Result = db.madnessRandom(80, 120);
                    console.log("Team2 Result - " + team2Result);
                }

                var winnerResult = team1Result > team2Result ? team1Result : team2Result;
                var looserResult = team1Result > team2Result ? team2Result : team1Result;
                db.query('update event_results set winner' + i + 'result = $1, looser' + i + 'result = $2 where eventid = $3',
                   [winnerResult, looserResult, id]);
            }
        });
    },

    calculateMarchMadnessTotal: function(result) {
         var numberOfSets = result.rows[0]["numberofsets"];
         var poolId = parseInt(result.rows[0]["id"]);
         for (var i = 1; i <= 3; i++) {
              var gridId = i == 3 && numberOfSets == 2 ? 2 : 1;
              var roundName = i == 3 ? "Round6" : "Round5";
              db.query('select epsc.memberid, epsc.membername, eprb.grid' + gridId + 'value as loosergridvalue, epcb.grid' + gridId + 'value as winnergridvalue, er.winner' + i + 'result as winnerresult, er.looser' + i + 'result as looserresult, \'' + roundName + '\' as roundname from event_pool_square_claims epsc ' +
                                  'inner join event_pools ep on ep.id = epsc.eventpoolid ' +
                                  'inner join event_pool_row_bets eprb on eprb.eventpoolid = ep.id and epsc.rownumber = eprb.rownumber ' +
                                  'inner join event_pool_column_bets epcb on epcb.eventpoolid = ep.id and epsc.columnnumber = epcb.columnnumber ' +
                                  'inner join events e on e.id = ep.eventid ' +
                                  'inner join event_results er on er.eventid = e.id where ep.id = ($1)', [poolId], function(result2) {
                     console.log(result2.rows);
                     for (var n = 0; n < result2.rows.length; n++) {
                        var row = result2.rows[n];
                        var membername = row["membername"];
                        var memberid = row["memberid"];
                        var winnerDigit = Math.abs(parseInt(row["winnerresult"])) % 10;
                        var looserDigit = Math.abs(parseInt(row["looserresult"])) % 10;
                        var winnerBet = row["winnergridvalue"];
                        var looserBet = row["loosergridvalue"];
                        console.log(winnerDigit + ", " + looserDigit + " == " + winnerBet + ", " + looserBet);
                        if (winnerDigit == winnerBet && looserDigit == looserBet) {
                            console.log("Membername " + membername + " win 1 point");
                            var total = 1;
                            var round5total = row["roundname"] == 'Round6' ? 0 : 1;
                            var round6total = row["roundname"] == 'Round6' ? 1 : 0;
                            db.query('update pool_totals set total = total + ($1), round5total = round5total + ($2), round6total = round6total + ($3) where eventpoolid = ($4) and memberid = ($5) and membername = ($6)',
                                [total, round5total, round6total, poolId, memberid, membername]);
                        }
                     }
                     db.query('update events set status = ($1) where id = ($2)', ['Calculated', poolId]);
              });
         }
    }
}