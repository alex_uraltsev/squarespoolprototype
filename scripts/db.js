var pg = require('pg');
pg.defaults.ssl = true;

module.exports = {
   query: function(text, values, cb) {
      pg.connect(process.env.DATABASE_URL, function(err, client, done) {
        client.query(text, values, function(err, result) {
          done();
          if (err) {
            return console.error(err);
          } else {
            if (cb) {
              cb(result);
            }
          }
        })
      });
   },

   queryCustomError: function(text, values, cb) {
     pg.connect(process.env.DATABASE_URL, function(err, client, done) {
       client.query(text, values, function(err, result) {
         done();
         if (cb) {
           cb(err, result);
         }
       })
     });
   },

   madnessRandom: function(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
   },
}