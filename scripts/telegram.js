var TelegramBot = require('node-telegram-bot-api');
var token = '331653346:AAECjGfg5yP3Y6hKtShEgAyCCy0ykvwRuUA';
var bot = new TelegramBot(token, {polling: false});

var S = require('string');
var constants = require('./constants.js');

module.exports = {

    handleMessage: function(chatId, text) {
        if (S(text).startsWith('/')) {
           this.handleCommand(chatId, S(text).strip('/').s);
        } else {
           bot.sendMessage(chatId, text);
        }
    },

    callback: function(chatId, data) {
        this.handleCommand(chatId, data);
    },

    handleCommand: function(chatId, command) {
        if (command == constants.TELEGRAM_COMMAND_START) {
            this.welcomeMessage(chatId);
        } else {
            bot.sendMessage(chatId, command);
        }
    },

    welcomeMessage: function(chatId) {
        bot.sendMessage(chatId, 'Select action', {
           reply_markup: JSON.stringify({
               inline_keyboard: [
                   [{
                       text: 'Create new pool',
                       callback_data: constants.TELEGRAM_COMMAND_CREATE
                   },
                   {
                       text: 'Manage my pools',
                       callback_data: constants.TELEGRAM_COMMAND_MANAGE
                   },
                   {
                       text: 'Where I am member?',
                       callback_data: constants.TELEGRAM_COMMAND_MEMBER
                   }
                   ]
               ]
           })
        })
    },

    sendWelcomeToPoolMessages: function(chatId, types) {
        var buttonsValue = "{\"inline_keyboard\":[";
        for (var i = 0; i < types.length; i++) {
               var eventType = types[i];
               buttonsValue += "[{\"text\": \"" + eventType.displayName + "\", \"callback_data\": \""
                  + constants.SELECT_EVENT_TYPE + eventType.name + "\"}]";
               if (i < types.length - 1) {
                  buttonsValue += ",";
               }
        }
        buttonsValue += "]}";

        bot.sendMessage(chatId,
                        'Welcome to pool setup.\nChoose event:',
                        {
                           reply_markup: JSON.parse(buttonsValue)
                        });
    },

    sendPoolNameMessage: function(recipientId) {
        bot.sendMessage(recipientId, 'Input your pool name.');
    },

    sendNumberOfSetMessage: function(recipientId, config) {
        var buttonsValue = "{\"inline_keyboard\":[";
        for (var i = 0; i < config.length; i++) {
            var configObj = config[i];
            buttonsValue += "[{\"text\": \"" + configObj.name + "\", \"callback_data\": \""
                   + constants.SELECT_NUMBER_OF_SET + configObj.value + "\"}]";
            if (i < config.length - 1) {
                buttonsValue += ",";
            }
        }
        buttonsValue += "]}";

        bot.sendMessage(recipientId, 'Select number of sets.', {
                                                                  reply_markup: JSON.parse(buttonsValue)
                                                                });
    },

    sendNumOfSquaresPerUser: function(recipientId) {
        var buttonsValue = "{\"inline_keyboard\":[";
        for (var i = 1; i <= 9; i++) {
             buttonsValue += "[{\"text\": \"" + i + "\", \"callback_data\": \""
                                    + constants.SELECT_NUM_OF_SQUARES_PER_USER + i + "\"}],";
        }
        buttonsValue += "[{\"text\": \"unlimited\", \"callback_data\": \""
                                            + constants.SELECT_NUM_OF_SQUARES_PER_USER + "unlimited" + "\"}]]}";
        bot.sendMessage(recipientId, 'Set the maximum number of squares per user.',
                                       {
                                       reply_markup: JSON.parse(buttonsValue)
                                       }
        );
    },

    sendVerifyPoolMessage: function(recipientId, messageText) {
        bot.sendMessage(recipientId, messageText, {
                                      reply_markup: JSON.stringify({
                                             inline_keyboard: [
                                                           [{
                                                              text: 'OK',
                                                              callback_data: constants.VERIFY_POOL + "OK"
                                                           },
                                                           {
                                                              text: 'Return to pool setup',
                                                              callback_data: constants.VERIFY_POOL + "Cancel"
                                                           }]
                                                           ]
                                             })
        });
    },

    sendMessageAboutNewPool: function(recipientId, chatlink) {
        bot.sendMessage(recipientId, "New pool have been created. Link to this pool - " + chatlink);
    },

    sendMemberView: function(recipientId, poolId, poolName, status) {
        var question = "Welcome to pool " + poolName + ". What do you want to do?";
        if (status == 'Active') {
            bot.sendMessage(recipientId, question, {
                                           reply_markup: JSON.stringify({
                                                inline_keyboard:[
                                                    [{
                                                      text: 'Grid status',
                                                      callback_data: constants.GRID_STATUS + poolId
                                                     },
                                                     {
                                                      text: 'Claim a square',
                                                      callback_data: constants.CLAIM_SQUARE + poolId
                                                     }]
                                                ]
                                           })
            });
        } else if (status == 'Locked') {
            bot.sendMessage(recipientId, question, {
                                          reply_markup: JSON.stringify({
                                               inline_keyboard:[
                                                   [{
                                                     text: 'Final grid',
                                                     callback_data: constants.FINAL_GRID + poolId
                                                    }]
                                               ]
                                          })
            });
        }
    },

    sendGridStatus: function(recipientId, poolId, url, isAdminView, isFinalGrid) {
         console.log("Image URL - " + url);
         var btn1 = {
               "text": isFinalGrid ? "Final Grid" : "Grid status",
               "callback_data": isFinalGrid ? constants.FINAL_GRID + poolId : constants.GRID_STATUS + poolId
         };

         var btn2 =  {
               "text": (isAdminView ? "Lock" : "Claim a square"),
               "callback_data": (isAdminView ? constants.LOCK_POOL + poolId : constants.CLAIM_SQUARE + poolId)
         }

         var btn3 = {
              "text": "Delete",
              "callback_data": constants.DELETE_POOL + poolId
         }

         var keyboardButtons = [];
         var row = [];

         row.push(btn1);
         if (!isFinalGrid) {
            row.push(btn2);
         }
         if (isFinalGrid && isAdminView) {
            row.push(btn3);
         }

         keyboardButtons.push(row);
         bot.sendPhoto(recipientId, url, {reply_markup: {
                                                         inline_keyboard: keyboardButtons
                                                        }
         });
    },

    sendClaimSquare: function(recipientId, poolId, memberName) {
         bot.sendMessage(recipientId, memberName + ", input grid square position (ex. A1, B3, etc):");
    },

    rejectClaimSquare: function(recipientId, poolId, errorText) {
        bot.sendMessage(recipientId, errorText, {
                                        reply_markup: {
                                                inline_keyboard: [
                                                    [
                                                         {
                                                           "text": "Grid status",
                                                           "callback_data": constants.GRID_STATUS + poolId
                                                         },
                                                         {
                                                           "text": "Claim a square",
                                                           "callback_data": constants.CLAIM_SQUARE + poolId
                                                         }
                                                    ]
                                                ]
                                        }
        });
    },

    sendMyPoolsView: function(recipientId, pools) {
         var buttons = "";
         for (var i = 0; i < pools.length; i++) {
                var poolInfo = pools[i];
                buttons += "[{" + "\"text\": \"" + poolInfo.name + " - " + poolInfo.status + "\", \"callback_data\": \""
                   + constants.VIEW_MY_POOL_PAYLOAD + poolInfo.uuid + "\"}]";
                if (i < pools.length - 1) {
                   buttons += ",";
                }
         }
         bot.sendMessage(recipientId, "My pools (latest 3)", {
                                                  reply_markup: {
                                                        inline_keyboard: [
                                                           JSON.parse(buttons)
                                                        ]

                                                  }
                                          });
    },

    sendWhereIAmMemberView: function(recipientId, pools) {
         var buttons = "";
         for (var i = 0; i < pools.length; i++) {
                var poolInfo = pools[i];
                buttons += "[{\"text\": \"" + poolInfo.name + " - " + poolInfo.status + "\", \"url\": \"" + poolInfo.chaturl + "\"}";
                if (i < pools.length - 1) {
                   buttons += ",";
                }
         }
         bot.sendMessage(recipientId, "You participate in pools (latest 3)", {
                                           reply_markup: {
                                                 inline_keyboard: [
                                                    JSON.parse(buttons)
                                                 ]

                                           }
                                   });
    },

     sendTotalPageUrl: function(recipientId, url) {
         bot.sendMessage(recipientId, "See table with totals by url " + url);
     },

     sendMatchesResultUrl: function(recipientId, url) {
         bot.sendMessage(recipientId, "See match results by url " + url);
     },

     sendAdminView: function(recipientId, poolId, poolName, status) {
         var question = "What do you want to do with pool?";
         if (status == 'Active') {
             bot.sendMessage(recipientId, question, {
                                                    reply_markup: {
                                                            inline_keyboard: [
                                                                [{
                                                                    "text":"Grid status",
                                                                    "callback_data": constants.GRID_STATUS + poolId + constants.ADMIN_SUFFIX
                                                                },
                                                                {
                                                                    "text":"Lock",
                                                                    "callback_data": constants.LOCK_POOL + poolId
                                                                }]
                                ]}});
         } else if (status == 'Locked') {
             bot.sendMessage(recipientId, question, {
                                                        reply_markup: {
                                                                inline_keyboard: [
                                                                    [{
                                                                        "text":"Results",
                                                                        "callback_data": constants.RESULTS + poolId
                                                                    },
                                                                    {
                                                                         "text":"Final Grid",
                                                                         "callback_data": constants.FINAL_GRID + poolId + constants.ADMIN_SUFFIX
                                                                    },
                                                                    {
                                                                        "text":"Delete",
                                                                        "callback_data": constants.DELETE_POOL + poolId
                                                                    }]
                                                                ]
                                                        }
                                                     });
         }
      },
}