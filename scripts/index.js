require('events').EventEmitter.defaultMaxListeners = Infinity;
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();

var fbMessenger = require('./fbMessenger.js');
var marchMadness = require('./marchMadness.js');
var constants = require('./constants.js');
var telegram = require('./telegram.js');
var db = require('./db.js');
var uuidV4 = require('uuid/v4');
var S = require('string');
var config = require('./env.json')[process.env.NODE_ENV || 'development'];
var grid = require('./grid.json');
var random_name = require('node-random-name');
var Jimp = require("jimp");

var memjs = require('memjs');
var memjsclient = memjs.Client.create(process.env.MEMCACHEDCLOUD_SERVERS, {
  username: process.env.MEMCACHEDCLOUD_USERNAME,
  password: process.env.MEMCACHEDCLOUD_PASSWORD
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(express.static('img'));
app.use("/tmp", express.static('tmp'));

app.listen((process.env.PORT || 3000));

// Server frontpage
app.get('/', function (req, res) {
    res.send('This is SquaresPoolBot Server');
});

// Facebook Webhook
app.get('/webhook', function (req, res) {
    //testJimp();
    if (req.query['hub.verify_token'] === 'bot_verify_token') {
        res.send(req.query['hub.challenge']);
    } else {
        res.send('Invalid verify token');
    }
    res.sendStatus(200);
});

// Pools Frontend
app.get('/pools', function (req, res) {
    allPools(res);
});

// Totals Frontend
app.get('/totals', function (req, res) {
    totalsForPool(req, res);
});

// Matches Frontend
app.get('/matches', function (req, res) {
    matchesForEvent(req, res);
});

// handler receiving messages
app.post('/webhook', function (req, res) {
    console.log("Request body: " + JSON.stringify(req.body));
    var commonEvents = [];

    if (req.body.entry != null) {
       events = req.body.entry[0].messaging;
       for (i = 0; i < events.length; i++) {
          var event = events[i];
          var commonEvent = {source: constants.FACEBOOK_BOT_SOURCE, event: event};
          commonEvents.push(commonEvent);
       }
    } else if (req.body.message != null) {
       var commonEvent = {source: constants.TELEGRAM_BOT_SOURCE, event: req.body.message};
       commonEvents.push(commonEvent);
    } else if (req.body.callback_query != null) {
        var commonEvent = {source: constants.TELEGRAM_BOT_SOURCE, event: req.body.callback_query};
        commonEvents.push(commonEvent);
    }

    for (i = 0; i < commonEvents.length; i++) {
       var commonEvent = commonEvents[i];
       console.log(commonEvent);

       var event = commonEvent.event;
       var source = commonEvent.source;
       var quickReply = null;
       var simpleMessage = null;
       var referral = null;
       var postback = null;
       var recipientId = null;

       if (isFacebookBot(source)) {
           recipientId = event.sender.id;
           if (event.message) {
              if (event.message.quick_reply) {
                quickReply = event.message.quick_reply.payload;
              } else if (event.message.text) {
                simpleMessage = event.message.text;
              }
           }
           postback = event.postback;
           referral = event.referral;
       } else if (isTelegramBot(source)) {
           if (event.message) {
              recipientId = event.message.chat.id;
              quickReply = event.data;
           } else {
              recipientId = event.chat.id;
              simpleMessage = event.text;
           }
       }

       if (quickReply != null) {
           processQuickReply(source, recipientId, quickReply);
       }
       if (postback != null) {
           processPostback(source, recipientId, postback);
       }
       if (referral != null) {
          processReferral(source, recipientId, referral);
       }
       if (simpleMessage != null) {
          processSimpleMessage(source, recipientId, simpleMessage);
       }
    }
    res.sendStatus(200);
});

function processQuickReply(source, recipientId, quickReply) {
    console.log("QuickReply received: " + quickReply + ". SenderID: " + recipientId);
    if (S(quickReply).startsWith(constants.GRID_STATUS)) {
       var poolId = S(quickReply).strip(constants.GRID_STATUS).s;
       var isAdminView = S(poolId).endsWith(constants.ADMIN_SUFFIX);
       if (isAdminView) {
           poolId = S(poolId).strip(constants.ADMIN_SUFFIX).s;
       }
       sendGridStatus(source, recipientId, poolId, isAdminView);
    } else if (S(quickReply).startsWith(constants.CLAIM_SQUARE)) {
       var poolId = S(quickReply).strip(constants.CLAIM_SQUARE).s;
       if (config.USER_PROFILE_MODE == 'Real') {
           sendClaimSquareUserProfile(source, recipientId, poolId);
       } else {
           var maleName = random_name({ first: true, gender: "male", random: customRandom});
           var femaleName = random_name({ first: true, gender: "female", random: customRandom});
           memberName = new Date().getTime() % 2 > 0 ? maleName : femaleName;
           sendClaimSquare(source, recipientId, poolId, memberName, true);
       }
    } else if (S(quickReply).startsWith(constants.SELECT_EVENT_TYPE)) {
       var eventType = S(quickReply).strip(constants.SELECT_EVENT_TYPE).s;
       var uuid = uuidV4();
       // put pool config to the cache
       var poolConfigObj = {poolId: uuid, poolType: eventType, poolName: "undefined", numOfSets: "0", numOfSquaresPerUser: "0"};
       memjsclient.set(source + "_" + recipientId.toString() + "_wizard", JSON.stringify(poolConfigObj),
          function(err, val) {
              if (isFacebookBot(source)) {
                  fbMessenger.sendPoolNameMessage(recipientId);
              } else if (isTelegramBot(source)) {
                  telegram.sendPoolNameMessage(recipientId);
              }
      }, 3600);
    } else if (S(quickReply).startsWith(constants.LOCK_POOL)) {
      var poolId = S(quickReply).strip(constants.LOCK_POOL).s;
      lockPool(source, recipientId, poolId);
    } else if (S(quickReply).startsWith(constants.RESULTS)) {
      var poolId = S(quickReply).strip(constants.RESULTS).s;
      showTotalForPool(source, recipientId, poolId);
    } else if (S(quickReply).startsWith(constants.FINAL_GRID)) {
      var poolId = S(quickReply).strip(constants.FINAL_GRID).s;
      var isAdminView = S(poolId).endsWith(constants.ADMIN_SUFFIX);
      if (isAdminView) {
          poolId = S(poolId).strip(constants.ADMIN_SUFFIX).s;
      }
      showFinalGrid(source, recipientId, poolId, isAdminView);
    } else if (S(quickReply).startsWith(constants.DELETE_POOL)) {
      var poolId = S(quickReply).strip(constants.DELETE_POOL).s;
      deletePool(source, recipientId, poolId);
    } else if (S(quickReply).startsWith(constants.SELECT_NUM_OF_SQUARES_PER_USER)) {
       var numOfSquares = S(quickReply).strip(constants.SELECT_NUM_OF_SQUARES_PER_USER).s;
       memjsclient.get(source + "_" + recipientId.toString() + "_wizard", function (err, value, key) {
              if (value != null) {
                 var poolConfigObj = JSON.parse(value.toString());
                 poolConfigObj.numOfSquaresPerUser = numOfSquares;
                 memjsclient.set(source + "_" + recipientId.toString() + "_wizard", JSON.stringify(poolConfigObj),
                                     function(err, val) {
                                          verifyPool(source, recipientId);
                                     }, 3600);
             } else {
                console.error("Error at selecting number of squares: there is no available pool for user " + recipientId);
             }
       });
    } else if (S(quickReply).startsWith(constants.VERIFY_POOL)) {
        var response = S(quickReply).strip(constants.VERIFY_POOL).s;
        if (response == 'OK') {
           memjsclient.get(source + "_" + recipientId.toString() + "_wizard", function (err, value, key) {
                 if (value != null) {
                     var poolConfigObj = JSON.parse(value.toString());
                     createPool(source, recipientId, poolConfigObj);
                     memjsclient.delete(source + "_" + recipientId.toString() + "_wizard", function(err, success) {});
                 } else {
                    console.error("Error at creation: there is no available pool for user " + recipientId);
                 }
           });
        } else {
           memjsclient.delete(source + "_" + recipientId.toString() + "_wizard", function(err, success) {
                   welcomeToPool(source, recipientId);
           });
        }
    } else if (S(quickReply).startsWith(constants.SELECT_NUMBER_OF_SET)) {
        var numOfSet = S(quickReply).strip(constants.SELECT_NUMBER_OF_SET).s;
        memjsclient.get(source + "_" + recipientId.toString() + "_wizard", function (err, value, key) {
            if (value != null) {
               var poolConfigObj = JSON.parse(value.toString());
               poolConfigObj.numOfSets = numOfSet;
               memjsclient.set(source + "_" + recipientId.toString() + "_wizard", JSON.stringify(poolConfigObj),
                                   function(err, val) {
                                       if (isFacebookBot(source)) {
                                            console.log("NOT IMPLEMENTED for Facebook Messenger");
                                       } else if (isTelegramBot(source)) {
                                            telegram.sendNumOfSquaresPerUser(recipientId);
                                       }
                                   }, 3600); //replace existing pool config
            } else {
               console.error("Error at selecting number of sets: there is no available pool for user " + recipientId);
            }
        });
    } else if (quickReply == constants.TELEGRAM_COMMAND_CREATE) {
       welcomeToPool(source, recipientId);
    } else if (quickReply == constants.TELEGRAM_COMMAND_MANAGE) {
       viewMyPoolList(source, recipientId);
    } else if (quickReply == constants.TELEGRAM_COMMAND_MEMBER) {
       whereIAmMember(source, recipientId);
    } else if (S(quickReply).startsWith(constants.VIEW_MY_POOL_PAYLOAD)) {
       var poolId = S(quickReply).strip(constants.VIEW_MY_POOL_PAYLOAD).s;
       createAdminView(source, recipientId, poolId);
    }
};

function processSimpleMessage(source, recipientId, text) {
    memjsclient.get(source + "_" + recipientId.toString() + "_wizard", function (err, value, key) {
        if (value != null && !S(text).startsWith('/')) {
            var obj = JSON.parse(value.toString());
            obj.poolName = text;
            memjsclient.set(source + "_" + recipientId.toString() + "_wizard", JSON.stringify(obj),
                function(err, val) {
                   selectNumberOfSets(source, recipientId);
                },
            3600);
        } else {
           memjsclient.get(source + "_" + recipientId.toString() + "_claim", function (err, value, key) {
               if (value != null) {
                   var obj = JSON.parse(value.toString());
                   persistGridSquare(source, obj.userId, obj.userName, obj.poolId, text);
               } else {
                  if (isFacebookBot(source)) {
                        fbMessenger.sendMessage(recipientId, {text: "Echo: " + text});
                  } else if (isTelegramBot(source)) {
                        if (S(text).startsWith('/')) {
                           var command = S(text).strip('/').s;
                           if (S(command).startsWith(constants.TELEGRAM_COMMAND_START)) {
                              var poolId = S(command).strip(constants.TELEGRAM_COMMAND_START).s;
                              if (poolId != "") {
                                poolId = poolId.trim();
                                console.log("PoolID: " + poolId);
                                createMemberView(source, recipientId, poolId);
                              } else {
                                telegram.handleCommand(recipientId, command);
                              }
                           } else if (command == constants.TELEGRAM_COMMAND_CREATE) {
                              welcomeToPool(source, recipientId);
                           } else if (command == constants.TELEGRAM_COMMAND_MANAGE) {
                              viewMyPoolList(source, recipientId);
                           } else if (command == constants.TELEGRAM_COMMAND_MEMBER) {
                              whereIAmMember(source, recipientId);
                           } else {
                              telegram.handleCommand(recipientId, command);
                           }
                        } else {
                           telegram.handleMessage(recipientId, text);
                        }
                  }
               }
           });
        }
    });
};

function processPostback(source, recipientId, postback) {
    console.log("Postback received: " + JSON.stringify(postback) + ". SenderID: " + recipientId);
    if (postback.payload == constants.NEW_POOL_PAYLOAD) {
        welcomeToPool(source, recipientId);
    } else if (postback.payload == constants.MANAGE_POOL_PAYLOAD) {
        viewMyPoolList(source, recipientId);
    } else if (postback.payload == constants.WHERE_I_AM_MEMBER_PAYLOAD) {
        whereIAmMember(source, recipientId);
    } else if (S(postback.payload).startsWith(constants.VIEW_MY_POOL_PAYLOAD)) {
        var poolId = S(postback.payload).strip(constants.VIEW_MY_POOL_PAYLOAD).s;
        createAdminView(source, recipientId, poolId);
    } else if (S(postback.payload).startsWith(constants.SELECT_NUMBER_OF_SET)) {
        var numOfSet = S(postback.payload).strip(constants.SELECT_NUMBER_OF_SET).s;
        memjsclient.get(source + "_" + recipientId.toString() + "_wizard", function (err, value, key) {
         if (value != null) {
             var poolConfigObj = JSON.parse(value.toString());
             poolConfigObj.numOfSets = numOfSet;
             memjsclient.set(source + "_" + recipientId.toString() + "_wizard", JSON.stringify(poolConfigObj),
                                 function(err, val) {
                                    fbMessenger.sendNumOfSquaresPerUser(recipientId);
                                 }, 3600); //replace existing pool config
         } else {
            console.error("Error at selecting number of sets: there is no available pool for user " + recipientId);
         }
      });
  }
};

function processReferral(source, recipientId, referral) {
   console.log("Referral received: " + JSON.stringify(referral) + ". SenderID: " + recipientId);
   createMemberView(source, recipientId, referral.ref);
};

function allPools(res) {
     db.query('select ep.id, ep.userid, ep.source, ep.name, et.name as eventtype, ep.numberOfSets, ep.numberOfSquarePerUser, ep.status, ep.uuid, ep.chaturl from event_pools ep inner join event_types et on ep.eventtypeid = et.id order by ep.id desc',[],
     function (result) {
           var pools = [];
           for (var i = 0; i < result.rows.length; i++) {
               var row = result.rows[i];
               pools.push({id : row["id"], userid: row["userid"], source: row["source"], name: row["name"], eventtype: row["eventtype"],
                           numberofsets: row["numberofsets"], numberofsquareperuser: row["numberofsquareperuser"],
                           status: row["status"], uuid: row["uuid"], chaturl: row["chaturl"]});
           }
           res.setHeader('Content-Type', 'application/json');
           res.status(200).json(pools);
     });
};

function totalsForPool(req, res) {
      db.query('select membername, total, round5total, round6total from pool_totals where eventpoolid = (select id from event_pools where status = \'Locked\' and uuid = ($1)) order by total desc',
          [req.query['poolId']], function (result) {
               var totals = [];
               for (var i = 0; i < result.rows.length; i++) {
                  var row = result.rows[i];
                  totals.push({membername : row["membername"], total: row["total"], round5total: row["round5total"], round6total: row["round6total"]});
               }
               res.setHeader('Content-Type', 'application/json');
               res.status(200).json(totals);
      });
}

function matchesForEvent(req, res) {
    console.log(req.query['poolId']);
    db.query('select winner1result, winner2result, winner3result, looser1result, looser2result, looser3result from event_results where eventid = (select e.id from events e inner join event_pools ep on ep.eventid = e.id where e.status = \'Calculated\' and ep.uuid = ($1))',
      [req.query['poolId']], function (result) {
           var matches = [];
           var row = result.rows[0];
           for (var i=1; i <= 3; i++) {
                var match = {round: i == 3 ? "Round 6" : "Round 5", winnerScore: row["winner" + i + "result"], looserScore: row["looser" + i + "result"]};
                matches.push(match);
           }
           res.setHeader('Content-Type', 'application/json');
           res.status(200).json(matches);
    });
}

function viewMyPoolList(source, recipientId) {
     db.query('select name, status, uuid, chaturl from event_pools where userid = ($1) and source = ($2) and (status = ($3) or status = ($4)) order by id desc limit 3',
      [recipientId.toString(), source, 'Active', 'Locked'], function (result) {
            var pools = [];
            for (var i = 0; i < result.rows.length; i++) {
                var row = result.rows[i];
                pools.push({name: row["name"], status: row["status"], uuid: row["uuid"], chaturl: row["chaturl"]});
            }
            if (pools.length > 0) {
                if (isFacebookBot(source)) {
                    fbMessenger.sendMyPoolsView(recipientId, pools);
                } else if (isTelegramBot(source)) {
                    telegram.sendMyPoolsView(recipientId, pools);
                }
            } else {
                if (isFacebookBot(source)) {
                    fbMessenger.sendMessage(recipientId, "There are no pools");
                } else if (isTelegramBot(source)) {
                    telegram.handleMessage(recipientId, "There are no pools");
                }
            }
     });
};

function whereIAmMember(source, userId) {
     //find memberid like '%userId' because used fake users under real facebook account
     //if use real accounts where condition need to change to memberid = userid
     var isFakeMember = config.USER_PROFILE_MODE != 'Real';

     if (isFakeMember) {
         db.query('select distinct ep.id, ep.name, ep.status, ep.uuid, ep.chaturl from event_pools ep inner join event_pool_square_claims epsc on epsc.eventpoolid = ep.id where epsc.memberid like ($1) and epsc.source = ($2) and (ep.status = ($3) or ep.status = ($4)) order by ep.id desc limit 3',
            ["%" + userId.toString(), source, 'Active', 'Locked'], function (result) {
               var pools = [];
               for (var i = 0; i < result.rows.length; i++) {
                    var row = result.rows[i];
                    pools.push({name: row["name"], status: row["status"], uuid: row["uuid"], chaturl: row["chaturl"]});
               }
               if (pools.length > 0) {
                   if (isFacebookBot(source)) {
                      fbMessenger.sendWhereIAmMemberView(userId, pools);
                   } else if (isTelegramBot(source)) {
                      telegram.sendWhereIAmMemberView(userId, pools);
                   }
               } else {
                   if (isFacebookBot(source)) {
                       fbMessenger.sendMessage(userId, "There are no pools");
                   } else if (isTelegramBot(source)) {
                       telegram.handleMessage(userId, "There are no pools");
                   }
               }
         });
     } else {
         db.query('select distinct ep.id, ep.name, ep.status, ep.uuid, ep.chaturl from event_pools ep inner join event_pool_square_claims epsc on epsc.eventpoolid = ep.id where epsc.memberid = ($1) and epsc.source = ($2) and (ep.status = ($3) or ep.status = ($4)) order by ep.id desc limit 3',
             [userId.toString(), source, 'Active', 'Locked'], function (result) {
                var pools = [];
                for (var i = 0; i < result.rows.length; i++) {
                     var row = result.rows[i];
                     pools.push({name: row["name"], status: row["status"], uuid: row["uuid"], chaturl: row["chaturl"]});
                }
                if (pools.length > 0) {
                   if (isFacebookBot(source)) {
                      fbMessenger.sendWhereIAmMemberView(userId, pools);
                   } else if (isTelegramBot(source)) {
                      telegram.sendWhereIAmMemberView(userId, pools);
                   }
                } else {
                   if (isFacebookBot(source)) {
                       fbMessenger.sendMessage(userId, "There are no pools");
                   } else if (isTelegramBot(source)) {
                       telegram.handleMessage(userId, "There are no pools");
                   }
                }
          });
     }
}

function welcomeToPool(source, recipientId) {
      db.query('SELECT name, displayname from event_types',[], function(result) {
           console.log(result.rows);
           var types = [];
           for (var i = 0; i < result.rows.length; i++) {
              var row = result.rows[i];
              console.log(row);
              types.push({name: row["name"], displayName: row["displayname"]});
           }
           console.log("Types: " + types);
           if (isFacebookBot(source)) {
             fbMessenger.sendWelcomeToPoolMessages(recipientId, types);
           } else if (isTelegramBot(source)) {
             telegram.sendWelcomeToPoolMessages(recipientId, types);
           }
      })
};

function selectNumberOfSets(source, recipientId) {
      db.query('SELECT name, value from number_of_sets',[], function (result){
           var config = [];
           for (var i = 0; i < result.rows.length; i++) {
               var row = result.rows[i];
               config.push({name: row["name"], value: row["value"]});
           }
           if (isFacebookBot(source)) {
              fbMessenger.sendNumberOfSetMessage(recipientId, config);
           } else if (isTelegramBot(source)) {
              telegram.sendNumberOfSetMessage(recipientId, config);
           }
      });
};


function verifyPool(source, recipientId) {
    memjsclient.get(source + "_" + recipientId.toString() + "_wizard", function (err, value, key) {
        if (value != null) {
            var poolConfigObj = JSON.parse(value.toString());
            if (isFacebookBot(source)) {
                fbMessenger.sendVerifyPoolMessage(recipientId, "Verify you want to create " + poolConfigObj.poolName + " for " + poolConfigObj.poolType +
                         " with " + poolConfigObj.numOfSets + " number of sets? Max squares per user are set to " + poolConfigObj.numOfSquaresPerUser + "."
                );
            } else if (isTelegramBot(source)) {
                telegram.sendVerifyPoolMessage(recipientId, "Verify you want to create " + poolConfigObj.poolName + " for " + poolConfigObj.poolType +
                        " with " + poolConfigObj.numOfSets + " number of sets? Max squares per user are set to " + poolConfigObj.numOfSquaresPerUser + "."
                );
            }
        } else {
           console.error("Error at selecting number of squares: there is no available pool for user " + recipientId);
        }
    });
};

function createPool(source, recipientId, poolConfigObj) {
      db.query('insert into events(name, userid, source) values ($1, $2, $3) returning id', [poolConfigObj.poolType, recipientId.toString(), source],
                  function(result) {
           //db.query('select max(id) as id from events where name = ($1) and userid = ($2)', [poolConfigObj.poolType, recipientId.toString()]);
           console.log("Inserted event id - " + result.rows[0]);
           var eventId = result.rows[0]["id"];

           db.query('select id from event_types where name = ($1)', [poolConfigObj.poolType], function(result2) {
               var eventTypeId = parseInt(result2.rows[0]["id"]);
               var link = isFacebookBot(source) ? (constants.FACEBOOK_CHAT_LINK + poolConfigObj.poolId) :
                              (constants.TELEGRAM_CHAT_LINK + poolConfigObj.poolId);
               var numOfSets = parseInt(poolConfigObj.numOfSets);
               var numOfSquares = poolConfigObj.numOfSquaresPerUser == "unlimited" ? -1 : parseInt(poolConfigObj.numOfSquaresPerUser);
               db.query('insert into event_pools(eventid, userid, source, name, eventTypeid, numberOfSets, numberOfSquarePerUser, status, uuid, chaturl) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)',
                      [eventId, recipientId.toString(), source, poolConfigObj.poolName, eventTypeId, numOfSets, numOfSquares, 'Active', poolConfigObj.poolId, link],
                      function (result) {
                         if (isFacebookBot(source)) {
                            fbMessenger.sendMessageAboutNewPool(recipientId, link);
                         } else if (isTelegramBot(source)) {
                            telegram.sendMessageAboutNewPool(recipientId, link);
                         }
               });
           });
      });
};

function createMemberView(source, recipientId, poolId) {
    //If 'Active' status -> show 'Grid Status' and 'Claim a square'
    //If 'Locked' status -> show 'Results'
     db.query('SELECT name, status from event_pools where uuid = ($1)', [poolId], function(result) {
         if (isFacebookBot(source)) {
            fbMessenger.sendMemberView(recipientId, poolId, result.rows[0]["name"], result.rows[0]["status"]);
         } else if (isTelegramBot(source)) {
            telegram.sendMemberView(recipientId, poolId, result.rows[0]["name"], result.rows[0]["status"]);
         }
     })
};

function createAdminView(source, recipientId, poolId) {
    //If 'Active' status -> show 'Grid Status' and 'Lock'
    //If 'Locked' status -> show 'Results' and 'Delete'
    db.query('SELECT name, status from event_pools where uuid = ($1)', [poolId], function (result) {
        if (isFacebookBot(source)) {
            fbMessenger.sendAdminView(recipientId, poolId, result.rows[0]["name"], result.rows[0]["status"]);
        } else if (isTelegramBot(source)) {
            telegram.sendAdminView(recipientId, poolId, result.rows[0]["name"], result.rows[0]["status"]);
        }
    });
};

function sendGridStatus(source, recipientId, poolId, isAdminView) {
   db.query('SELECT membername, rownumber, columnnumber from event_pool_square_claims where eventpoolid = (select ep.id from event_pools ep where ep.uuid = ($1))',
               [poolId], function(result) {
        var squares = [];
        for (var i = 0; i < result.rows.length; i++) {
           var row = result.rows[i];
           squares.push({user: row["membername"], grid: row["columnnumber"] + row["rownumber"]});
        }
         if (squares.length == 0) {
             if (isFacebookBot(source)) {
                fbMessenger.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/emptyGrid.jpg", isAdminView, false);
             } else if (isTelegramBot(source)) {
                telegram.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/emptyGrid.jpg", isAdminView, false);
             }
         } else {
             Jimp.read(config.WEBSERVER_URI + "/emptyGrid.jpg", function (err, image) {
                if (err) {
                   throw err;
                }
                Jimp.loadFont(constants.TREBUCHET_MS_FONT).then(function (font) {
                  for (i = 0; i < squares.length; i++) {
                     var square = squares[i];
                     var key = square.grid;
                     var x = grid[key].x;
                     var y = grid[key].y;
                     console.log("Print " + square.user + " in position [" + x + "," + y + "]");
                     image.print(font, parseInt(x), parseInt(y), square.user);
                  }
                  var fileName = poolId + recipientId.toString() + "_grid.jpg";
                  var path = "tmp/" + fileName;
                  image.write(path);
                  if (isFacebookBot(source)) {
                    fbMessenger.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/" + path, isAdminView, false);
                  } else if (isTelegramBot(source)) {
                    telegram.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/" + path, isAdminView, false);
                  }
                });
             });
         }
   });
};

function persistGridSquare(source, userId, userName, poolId, text) {

   var positions = parseGridPosition(text);
   var isFakeMember = config.USER_PROFILE_MODE != 'Real';
   if (positions.row == "-1" || positions.column == "-1") {
       if (isFacebookBot(source)) {
            fbMessenger.rejectClaimSquare(userId, poolId, "Wrong format for grid position");
       } else if (isTelegramBot(source)) {
            telegram.rejectClaimSquare(userId, poolId, "Wrong format for grid position");
       }
   } else {
        db.query('select id from event_pools where uuid = ($1)', [poolId], function (result) {
             var eventPoolId = parseInt(result.rows[0]["id"]);
             db.queryCustomError('insert into event_pool_square_claims(eventpoolid, rownumber, columnnumber, membername, memberid, source) values ($1, $2, $3, $4, $5, $6)',
                   [eventPoolId, positions.row, positions.column, userName, isFakeMember ? userName + userId : userId, source],
                   function (err, result2) {
                         if (err) {
                            console.error('error happened during query', err);
                            memjsclient.delete(source + "_" + userId.toString() + "_claim");
                            if (isFacebookBot(source)) {
                                fbMessenger.rejectClaimSquare(userId, poolId, userName + ", this square unavailable. Please select another square.");
                            } else if (isTelegramBot(source)) {
                                telegram.rejectClaimSquare(userId, poolId, userName + ", this square unavailable. Please select another square.");
                            }
                            return;
                         }
                         memjsclient.delete(source + "_" + userId.toString() + "_claim");
                         sendGridStatus(source, userId, poolId);
                         //insert to pool_totals
                         db.query('insert into pool_totals (eventpoolid, memberid, membername, source, total, round5total, round6total) ' +
                              ' select ' + eventPoolId + ', \'' + (isFakeMember ? userName + userId : userId) + '\', \'' + userName + '\', \'' + source + '\', 0, 0, 0 where not exists (select pt.id from pool_totals pt where pt.eventpoolid = ($1) and pt.memberid = ($2) and pt.membername = ($3) and pt.source = ($4))',
                              [eventPoolId, isFakeMember ? userName + userId : userId, userName, source]);
               });


        });
   }
};

function sendClaimSquare(source, recipientId, poolId, memberName, isFakeMember) {
     db.query('select numberofsquareperuser from event_pools where uuid = ($1) and userId = ($2)', [poolId, recipientId.toString()],
        function (result) {
           var limitSquares = parseInt(result.rows[0]["numberofsquareperuser"]);
           db.query('select count(epsc.id) as cnt from event_pool_square_claims epsc, event_pools ep where epsc.eventpoolid = ep.id and ep.uuid = ($1) and epsc.memberid = ($2)',
                [poolId, isFakeMember ? memberName + recipientId.toString() : recipientId.toString()], function (result2) {
                var claimed = parseInt(result2.rows[0]["cnt"]);
                if (claimed < limitSquares || limitSquares == -1) {
                   //save claim request for user
                   var claimRequest = {userId: recipientId, poolId: poolId, userName: memberName};
                   memjsclient.set(source + "_" + recipientId.toString() + "_claim", JSON.stringify(claimRequest),
                       function(err, val) {
                          if (isFacebookBot(source)) {
                            fbMessenger.sendClaimSquare(recipientId, poolId, memberName);
                          } else if (isTelegramBot(source)) {
                            telegram.sendClaimSquare(recipientId, poolId, memberName);
                          }
                       }, 300);
                } else {
                    fbMessenger.rejectClaimSquare(recipientId, poolId, memberName + ", you reached limit of squares!");
                    return;
                }
           });
     });
};

function lockPool(source, recipientId, poolId) {
    db.query('update event_pools set status = ($1) where uuid = ($2)', ['Locked', poolId],
        function() {
            db.query('select id, numberofsets from event_pools where uuid = ($1)', [poolId], generateSetsOfNumbers);
            //specific for March Madness!!!
            db.query('select e.id from events e inner join event_pools ep on ep.eventid = e.id where ep.uuid = ($1)', [poolId],
                marchMadness.generateMarchMadnessResults);
            createAdminView(source, recipientId, poolId);
    });
}

function showTotalForPool(source, recipientId, poolId) {
   db.query('select e.status from events e inner join event_pools ep on ep.eventid = e.id where ep.uuid = ($1)', [poolId],
     function(result) {
        var status = result.rows[0]["status"];
        if (status != 'Calculated') {
           console.log("Calculate Totals!!!");
           db.query('select id, numberofsets from event_pools where uuid = ($1)', [poolId], marchMadness.calculateMarchMadnessTotal);
        }
   });
   var url = config.WEBSERVER_URI + "/totals.html?poolId=" + poolId;
   if (isFacebookBot(source)) {
      fbMessenger.sendTotalPageUrl(recipientId, url);
   } else if (isTelegramBot(source)) {
      telegram.sendTotalPageUrl(recipientId, url);
   }

   var url2 = config.WEBSERVER_URI + "/matches.html?poolId=" + poolId;
   if (isFacebookBot(source)) {
      fbMessenger.sendMatchesResultUrl(recipientId, url2);
   } else if (isTelegramBot(source)) {
      telegram.sendMatchesResultUrl(recipientId, url2);
   }
   createAdminView(source, recipientId, poolId);
}

function showFinalGrid(source, recipientId, poolId, isAdminView) {
   db.query('select numberofsets from event_pools where uuid = ($1)', [poolId], function (result) {
       var numberOfSets = result.rows[0]["numberofsets"];
       db.query('SELECT rownumber, grid1value, grid2value, grid3value, grid4value, grid5value, grid6value from event_pool_row_bets where eventpoolid = (select ep.id from event_pools ep where ep.uuid = ($1))',
          [poolId], function (result2) {
             var rowBets = [];
             for (var i = 0; i < result2.rows.length; i++) {
                  var row = result2.rows[i];
                  rowBets.push({rowNumber: row["rownumber"], grid1Value: row["grid1value"], grid2Value: row["grid2value"]});
             }
             db.query('SELECT columnnumber, grid1value, grid2value, grid3value, grid4value, grid5value, grid6value from event_pool_column_bets where eventpoolid = (select ep.id from event_pools ep where ep.uuid = ($1))',
             [poolId], function (result3) {
                   var columnBets = [];
                   for (var i = 0; i < result3.rows.length; i++) {
                      var row = result3.rows[i];
                      columnBets.push({columnNumber:  row["columnnumber"], grid1Value: row["grid1value"], grid2Value: row["grid2value"]});
                   }
                   db.query('SELECT membername, rownumber, columnnumber from event_pool_square_claims where eventpoolid = (select ep.id from event_pools ep where ep.uuid = ($1))',
                   [poolId], function (result4) {
                        var squares = [];
                        for (var i = 0; i < result4.rows.length; i++) {
                           var row = result4.rows[i];
                           squares.push({user: row["membername"], grid: row["columnnumber"] + row["rownumber"]});
                        }
                        if (numberOfSets == '1') {
                             Jimp.read(config.WEBSERVER_URI + "/emptyResultGrid.jpg", function (err, image) {
                                if (err) {
                                   throw err;
                                }
                                Jimp.loadFont(constants.TREBUCHET_MS_FONT).then(function (font) {
                                  for (i = 0; i < rowBets.length; i++) {
                                     var bet = rowBets[i];
                                     var x = grid[bet.rowNumber + "Header"].x;
                                     var y = grid[bet.rowNumber + "Header"].y;
                                     image.print(font, parseInt(x), parseInt(y), bet.grid1Value.toString());
                                  }
                                  for (i = 0; i < columnBets.length; i++) {
                                     var bet = columnBets[i];
                                     var x = grid[bet.columnNumber + "Header"].x;
                                     var y = grid[bet.columnNumber + "Header"].y;
                                     image.print(font, parseInt(x), parseInt(y), bet.grid1Value.toString());
                                  }
                                  for (i = 0; i < squares.length; i++) {
                                     var square = squares[i];
                                     var key = square.grid;
                                     var x = grid[key].x;
                                     var y = grid[key].y;
                                     image.print(font, parseInt(x), parseInt(y), square.user);
                                  }

                                  var fileName = poolId + recipientId.toString() + "_final_grid.jpg";
                                  var path = "tmp/" + fileName;
                                  image.write(path);
                                  if (isFacebookBot(source)) {
                                    fbMessenger.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/" + path, isAdminView, true);
                                  } else if (isTelegramBot(source)) {
                                    telegram.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/" + path, isAdminView, true);
                                  }
                                });
                             });
                      } else if (numberOfSets == '2') {
                          Jimp.read(config.WEBSERVER_URI + "/emptyResultGrid2.jpg", function (err, image) {
                              if (err) {
                                 throw err;
                              }
                              Jimp.loadFont(constants.TREBUCHET_MS_FONT).then(function (font) {
                                for (i = 0; i < rowBets.length; i++) {
                                   var bet = rowBets[i];
                                   var x1 = grid[bet.rowNumber + "Header"].x1;
                                   var y1 = grid[bet.rowNumber + "Header"].y1;
                                   var x2 = grid[bet.rowNumber + "Header"].x2;
                                   var y2 = grid[bet.rowNumber + "Header"].y2;
                                   image.print(font, parseInt(x1), parseInt(y1), bet.grid1Value.toString());
                                   image.print(font, parseInt(x2), parseInt(y2), bet.grid2Value.toString());
                                }
                                for (i = 0; i < columnBets.length; i++) {
                                   var bet = columnBets[i];
                                   var x1 = grid[bet.columnNumber + "Header"].x1;
                                   var y1 = grid[bet.columnNumber + "Header"].y1;
                                   var x2 = grid[bet.columnNumber + "Header"].x2;
                                   var y2 = grid[bet.columnNumber + "Header"].y2;
                                   image.print(font, parseInt(x1), parseInt(y1), bet.grid1Value.toString());
                                   image.print(font, parseInt(x2), parseInt(y2), bet.grid2Value.toString());
                                }
                                for (i = 0; i < squares.length; i++) {
                                   var square = squares[i];
                                   var key = square.grid;
                                   var x = grid[key].x;
                                   var y = grid[key].y;
                                   image.print(font, parseInt(x), parseInt(y), square.user);
                                }

                                var fileName = poolId + recipientId.toString() + "_final_grid.jpg";
                                var path = "tmp/" + fileName;
                                image.write(path);
                                if (isFacebookBot(source)) {
                                    fbMessenger.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/" + path, isAdminView, true);
                                } else if (isTelegramBot(source)) {
                                    telegram.sendGridStatus(recipientId, poolId, config.WEBSERVER_URI + "/" + path, isAdminView, true);
                                }
                              });
                          });
                      }
                   });
             });
        });
   });
}

function deletePool(source, recipientId, poolId) {
    db.query('update event_pools set status = ($1) where uuid = ($2)', ['Deleted', poolId]);
}

function generateSetsOfNumbers(result) {
     var id = result.rows[0]["id"];
     var setOfNumbers = result.rows[0]["numberofsets"];

     var arrR = [0,1,2,3,4,5,6,7,8,9];
     var arrC = [0,1,2,3,4,5,6,7,8,9];
     var letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

     for (var i = 1; i <= setOfNumbers; i++) {
        var arrResR = shuffle(arrR);
        var arrResC = shuffle(arrC);

        for (var j = 1; j <= 10; j++) {
            console.log("i = " + i + ", j = " + j + ", gridValueR = " + arrResR[j-1]);
            console.log("i = " + i + ", j = " + j + ", gridValueC = " + arrResC[j-1]);
            if (i == 1) {
                db.query('insert into event_pool_row_bets(eventpoolid, rownumber, grid1value) values($1,$2,$3)', [id, j.toString(), arrResR[j-1]]);
                db.query('insert into event_pool_column_bets(eventpoolid, columnnumber, grid1value) values($1,$2,$3)', [id, letters[j-1], arrResC[j-1]]);
            } else {
                db.query('update event_pool_row_bets set grid' + i + 'value = ($1) where eventpoolid = ($2) and rownumber = ($3)', [arrResR[j-1], id, j.toString()]);
                db.query('update event_pool_column_bets set grid' + i + 'value = ($1) where eventpoolid = ($2) and columnnumber = ($3)', [arrResR[j-1], id, letters[j-1]]);
            }
        }
     }
}

function sendClaimSquareUserProfile(source, recipientId, poolId) {
     memjsclient.get(source + "_" + recipientId.toString() + "_profile", function (err, value, key) {
        if (value != null) {
            var obj = JSON.parse(value.toString());
            sendClaimSquare(source, recipientId, poolId, obj.firstName, false);
        } else {
            if (isFacebookBot(source)) {
                request({
                    url: 'https://graph.facebook.com/v2.6/' + recipientId.toString(),
                    qs: {access_token: process.env.PAGE_ACCESS_TOKEN},
                    method: 'GET'
                }, function(error, response, body) {
                    if (error) {
                        console.log('Error at getting user profile: ', error);
                    } else if (response.body.error) {
                        console.log('Error: ', response.body.error);
                    }

                    var obj = JSON.parse(body);
                    var userProfile = {firstName: obj["first_name"], lastName: obj["last_name"], pic: obj["profile_pic"]};
                    sendClaimSquare(source, recipientId, poolId, userProfile.firstName, false);
                    memjsclient.set(source + "_" + recipientId.toString() + "_profile", JSON.stringify(userProfile),
                         function(err, val) {}, 60000);
                });
            } else if (isTelegramBot(source)) {
                var chat = bot.getChat(recipientId);
                var userProfile = {firstName: chat.first_name, lastName: chat.last_name};
                sendClaimSquare(source, recipientId, poolId, userProfile.firstName, false);
                memjsclient.set(source + "_" + recipientId.toString() + "_profile", JSON.stringify(userProfile),
                     function(err, val) {}, 60000);
            }
        }
    });
}

function customRandom() {
  return Math.floor(Math.random() * 11) / 100;
}

function parseGridPosition(text) {
  if (text.length > 3) {
     return {column: "-1", row: "-1"};
  } else if (text.length == 3) {
     return {column: text.charAt(0).toUpperCase(), row: (text.charAt(1) + text.charAt(2)).toUpperCase()};
  } else {
     return {column: text.charAt(0).toUpperCase(), row: text.charAt(1).toUpperCase()};
  }
}

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items The array containing the items.
 */
function shuffle(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
    return a;
}

function isFacebookBot(source) {
    return source == constants.FACEBOOK_BOT_SOURCE;
}

function isTelegramBot(source) {
    return source == constants.TELEGRAM_BOT_SOURCE;
}

//function testJimp() {
//    var name = random_name({ first: true, gender: "female", random: customRandom});
//    //console.log("Random name - " + name);
//    name = "Алекс";
//
//    Jimp.read(config.WEBSERVER_URI + "/emptyResultGrid2.jpg", function (err, image) {
//        if (err) {
//            throw err;
//        }
//        //Jimp.loadFont("./fnt/arialItalic.fnt");
//        Jimp.loadFont("fnt/trebuchetMS12.fnt").then(function (font) {
//           image.print(font, 7, 35, name);
//           image.print(font, 30, 45, name);
//           image.print(font, 7, 65, name);
//           image.print(font, 30, 80, name);
//           image.print(font, 7, 100, name);
//           image.print(font, 30, 110, name);
//           image.print(font, 7, 130, name);
//           image.print(font, 30, 140, name);
//           image.print(font, 7, 165, name);
//           image.print(font, 30, 175, name);
//           image.print(font, 7, 195, name);
//           image.print(font, 30, 205, name);
//           image.print(font, 7, 225, name);
//           image.print(font, 30, 235, name);
//           image.print(font, 7, 260, name);
//           image.print(font, 30, 270, name);
//           image.print(font, 7, 290, name);
//           image.print(font, 30, 300, name);
//           image.print(font, 7, 322, name);
//           image.print(font, 30, 333, name);
//           var fileName = "tmp/2122121dfdfsdfsd_tempGrid.jpg";
//           image.write(fileName);
//        });
//    });
//}