var request = require('request');
var constants = require('./constants.js');

module.exports = {
    // generic function sending messages
     sendMessage: function(recipientId, message) {
        request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token: process.env.PAGE_ACCESS_TOKEN},
            method: 'POST',
            json: {
                recipient: {id: recipientId},
                message: message,
            }
        }, function(error, response, body) {
            if (error) {
                console.log('Error sending message: ', error);
            } else if (response.body.error) {
                console.log('Error: ', response.body.error);
            }
        });
    },

    sendMyPoolsView: function(recipientId, pools) {
         var buttonsValue = "[";
         for (var i = 0; i < pools.length; i++) {
                var poolInfo = pools[i];
                buttonsValue += "{" + "\"type\": \"postback\", \"title\": \"" + poolInfo.name + " - " + poolInfo.status + "\", \"payload\": \""
                   + constants.VIEW_MY_POOL_PAYLOAD + poolInfo.uuid + "\"}";
                if (i < pools.length - 1) {
                   buttonsValue += ",";
                }
         }
         buttonsValue += "]";
         message = {
                   "attachment": {
                         "type": "template",
                         "payload": {
                             "template_type": "button",
                             "text": "My pools (latest 3)",
                             "buttons": JSON.parse(buttonsValue)
                         }
                   }
         };
        console.log(JSON.stringify(message));
        this.sendMessage(recipientId, message);
    },

    sendWhereIAmMemberView: function(recipientId, pools) {
         var buttonsValue = "[";
         for (var i = 0; i < pools.length; i++) {
                var poolInfo = pools[i];
                buttonsValue += "{" + "\"type\": \"web_url\", \"title\": \"" + poolInfo.name + " - " + poolInfo.status + "\", \"url\": \""
                   + poolInfo.chaturl + "\"}";
                if (i < pools.length - 1) {
                   buttonsValue += ",";
                }
         }
         buttonsValue += "]";
         message = {
                   "attachment": {
                         "type": "template",
                         "payload": {
                             "template_type": "button",
                             "text": "You participate in pools (latest 3)",
                             "buttons": JSON.parse(buttonsValue)
                         }
                   }
         };
        console.log(JSON.stringify(message));
        this.sendMessage(recipientId, message);
    },

    sendWelcomeToPoolMessages: function(recipientId, eventTypes) {
              var buttonsValue = "[";
              for (var i = 0; i < eventTypes.length; i++) {
                    var eventType = eventTypes[i];
                    buttonsValue += "{" + "\"content_type\": \"text\", \"title\": \"" + eventType.displayName + "\", \"payload\": \""
                       + constants.SELECT_EVENT_TYPE + eventType.name + "\"}";
                    if (i < eventTypes.length - 1) {
                       buttonsValue += ",";
                    }
              }
              buttonsValue += "]";

              this.sendMessage(recipientId, {
                                    text: "Welcome to pool setup.\nChoose event:",
                                    quick_replies: JSON.parse(buttonsValue)
              });
        },

     sendNumberOfSetMessage: function(recipientId, config) {
        var buttonsValue = "[";
        for (var i = 0; i < config.length; i++) {
            var configObj = config[i];
            buttonsValue += "{" + "\"type\": \"postback\", \"title\": \"" + configObj.name + "\", \"payload\": \""
                   + constants.SELECT_NUMBER_OF_SET + configObj.value + "\"}";
            if (i < config.length - 1) {
                buttonsValue += ",";
            }
        }
        buttonsValue += "]";

        this.sendMessage(recipientId, {text: "Set the number of sets."})
        message = {
               "attachment": {
                     "type": "template",
                     "payload": {
                         "template_type": "generic",
                         "elements": [{
                             "title": "Number of sets",
                             "buttons": JSON.parse(buttonsValue)
                         }]
                     }
               }
        };
        console.log(JSON.stringify(message));
        this.sendMessage(recipientId, message);
     },

     sendNumOfSquaresPerUser: function(recipientId) {
        var buttonsValue = "[";
        for (var i = 1; i <= 9; i++) {
             buttonsValue += "{" + "\"content_type\": \"text\", \"title\": \"" + i + "\", \"payload\": \""
                                    + constants.SELECT_NUM_OF_SQUARES_PER_USER + i + "\"},";
        }
        buttonsValue += "{" + "\"content_type\": \"text\", \"title\": \"unlimited\", \"payload\": \""
                                            + constants.SELECT_NUM_OF_SQUARES_PER_USER + "unlimited" + "\"}]";
        this.sendMessage(recipientId, {text: "Set the maximum number of squares per user.",
                                      quick_replies: JSON.parse(buttonsValue)
        });
     },

     sendVerifyPoolMessage: function(recipientId, messageText) {
        this.sendMessage(recipientId, {text: messageText,
                                       quick_replies: [
                                       {
                                         content_type: "text",
                                         title: "OK",
                                         payload: constants.VERIFY_POOL + "OK"
                                       },
                                       {
                                         content_type: "text",
                                         title: "Return to pool setup",
                                         payload: constants.VERIFY_POOL + "Cancel"
                                       }]
        });
     },

     sendPoolNameMessage: function(recipientId) {
        this.sendMessage(recipientId, {text: "Input your pool name."});
     },

     sendMessageAboutNewPool: function(recipientId, chatlink) {
        this.sendMessage(recipientId, {text: "New pool have been created. Link to this pool - " + chatlink});
     },

     sendMemberView: function(recipientId, poolId, poolName, status) {
        var question = "Welcome to pool " + poolName + ". What do you want to do?";
        if (status == 'Active') {
            this.sendMessage(recipientId, {text: question,
                                           quick_replies: [
                                                {
                                                  "content_type":"text",
                                                  "title":"Grid status",
                                                  "payload": constants.GRID_STATUS + poolId
                                                },
                                                {
                                                  "content_type":"text",
                                                  "title":"Claim a square",
                                                  "payload": constants.CLAIM_SQUARE + poolId
                                                }
                                           ]
            });
        } else if (status == 'Locked') {
            this.sendMessage(recipientId, {text: question,
                        quick_replies: [
                             {
                               "content_type":"text",
                               "title":"Final Grid",
                               "payload": constants.FINAL_GRID + poolId
                             }
                        ]
            });
        }
     },

     sendAdminView: function(recipientId, poolId, poolName, status) {
        var question = "What do you want to do with pool?";
        if (status == 'Active') {
            this.sendMessage(recipientId, {text: question,
                        quick_replies: [
                             {
                               "content_type":"text",
                               "title":"Grid status",
                               "payload": constants.GRID_STATUS + poolId + constants.ADMIN_SUFFIX
                             },
                             {
                               "content_type":"text",
                               "title":"Lock",
                               "payload": constants.LOCK_POOL + poolId
                             }
                        ]
            });
        } else if (status == 'Locked') {
            this.sendMessage(recipientId, {text: question,
                quick_replies: [
                     {
                       "content_type":"text",
                       "title":"Results",
                       "payload": constants.RESULTS + poolId
                     },
                     {
                        "content_type":"text",
                        "title":"Final Grid",
                        "payload": constants.FINAL_GRID + poolId + constants.ADMIN_SUFFIX
                     },
                     {
                       "content_type":"text",
                       "title":"Delete",
                       "payload": constants.DELETE_POOL + poolId
                     }
                ]
            });
        }
     },

     sendGridStatus: function(recipientId, poolId, url, isAdminView, isFinalGrid) {
         console.log("Image URL - " + url);
         var reply1 = {
               "content_type":"text",
               "title": isFinalGrid ? "Final Grid" : "Grid status",
               "payload": isFinalGrid ? constants.FINAL_GRID + poolId : constants.GRID_STATUS + poolId
         };

         var reply2 =  {
               "content_type":"text",
               "title": (isAdminView ? "Lock" : "Claim a square"),
               "payload": (isAdminView ? constants.LOCK_POOL + poolId : constants.CLAIM_SQUARE + poolId)
         }

         var reply3 = {
              "content_type":"text",
              "title": "Delete",
              "payload": constants.DELETE_POOL + poolId
         }

         var quick_replies = [];
         quick_replies.push(reply1);
         if (!isFinalGrid) {
            quick_replies.push(reply2);
         }
         if (isFinalGrid && isAdminView) {
            quick_replies.push(reply3);
         }

         message = {
               quick_replies: quick_replies,
               attachment: {
                    "type": "image",
                    "payload": {
                       "url": url
                    }
               }
         };
         this.sendMessage(recipientId, message);
     },

     sendClaimSquare: function(recipientId, poolId, memberName) {
         this.sendMessage(recipientId, {text: memberName + ", input grid square position (ex. A1, B3, etc):"});
     },

     rejectClaimSquare: function(recipientId, poolId, errorText) {
        this.sendMessage(recipientId, {text: errorText,
                                        quick_replies: [
                                             {
                                               "content_type":"text",
                                               "title":"Grid status",
                                               "payload": constants.GRID_STATUS + poolId
                                             },
                                             {
                                               "content_type":"text",
                                               "title":"Claim a square",
                                               "payload": constants.CLAIM_SQUARE + poolId
                                             }
                                        ]
        });
     },

     sendTotalPageUrl: function(recipientId, url) {
         this.sendMessage(recipientId, {text: "See table with totals by url " + url});
     },

     sendMatchesResultUrl: function(recipientId, url) {
         this.sendMessage(recipientId, {text: "See match results by url " + url});
     }

}